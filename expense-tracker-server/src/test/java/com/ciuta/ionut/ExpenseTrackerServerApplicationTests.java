package com.ciuta.ionut;

import com.ciuta.ionut.service.UserService;
import com.ciuta.ionut.service.UserTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExpenseTrackerServerApplicationTests {
	@Autowired
	private UserTypeService userTypeService;

	@Autowired
	private UserService userService;

	@Test
	public void userTypeTest() {
		System.out.println(userTypeService.findAll());
	}

	@Test
	public void userTest() {
		System.out.println(userService.findAll());
	}
}
