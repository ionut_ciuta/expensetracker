package com.ciuta.ionut.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Ionut Ciuta on 11/21/2016.
 */
@Entity
@Table(name = "user_type")
public class UserType extends AbstractEntity {
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean active;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_type_permission",
            joinColumns = @JoinColumn(name = "user_type_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Set<Permission> permissions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "UserType{" +
                "name='" + name + '\'' +
                ", active=" + active +
                ", permissions=" + permissions +
                '}';
    }
}
