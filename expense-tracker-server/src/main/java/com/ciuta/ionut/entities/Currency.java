package com.ciuta.ionut.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Entity
@Table(name = "currency")
public class Currency extends AbstractEntity {
    @Column(nullable = false)
    private String title;

    @Column(name = "iso_code", nullable = false)
    private String iso;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "title='" + title + '\'' +
                ", iso='" + iso + '\'' +
                '}';
    }
}
