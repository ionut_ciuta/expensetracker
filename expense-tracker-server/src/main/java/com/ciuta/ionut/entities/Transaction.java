package com.ciuta.ionut.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Entity
@Table(name = "transaction")
public class Transaction extends AbstractEntity {
    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private BigDecimal amount = BigDecimal.ZERO;

    @Column(name = "trans_data", nullable = false)
    private Date date = new Date();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cat_id", nullable = false)
    private TransactionCategory category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "acc_id", nullable = false)
    private Account account;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TransactionCategory getCategory() {
        return category;
    }

    public void setCategory(TransactionCategory category) {
        this.category = category;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", category=" + category +
                ", account=" + account +
                '}';
    }
}
