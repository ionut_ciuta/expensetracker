package com.ciuta.ionut.entities;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Entity
@Table(name = "account")
public class Account extends AbstractEntity{
    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private Boolean active = Boolean.TRUE;

    @Column
    private String description;

    @Column(nullable = false)
    private String type;

    @Column(name = "init_amount", nullable = false)
    private BigDecimal amount = BigDecimal.ZERO;

    @Column(name = "remainder", nullable = false)
    private BigDecimal remaining = BigDecimal.ZERO;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "curr_id", nullable = false)
    private Currency currency;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getRemaining() {
        return remaining;
    }

    public void setRemaining(BigDecimal remaining) {
        this.remaining = remaining;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Account{" +
                "title='" + title + '\'' +
                ", active=" + active +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", remaining=" + remaining +
                ", user=" + user +
                ", currency=" + currency +
                '}';
    }
}
