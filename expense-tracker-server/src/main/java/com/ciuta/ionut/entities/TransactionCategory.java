package com.ciuta.ionut.entities;

import javax.persistence.*;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Entity
@Table(name = "transaction_category")
public class TransactionCategory extends AbstractEntity{
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean active = Boolean.TRUE;

    @Column
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TransactionCategory{" +
                "name='" + name + '\'' +
                ", active=" + active +
                ", description='" + description + '\'' +
                ", user=" + user +
                '}';
    }
}
