package com.ciuta.ionut.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Ionut Ciuta on 11/21/2016.
 */
@Entity
@Table(name = "permission")
public class Permission extends AbstractEntity {
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "name='" + name + '\'' +
                ", active=" + active +
                '}';
    }
}
