package com.ciuta.ionut.dao;

import com.ciuta.ionut.entities.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * Ionut Ciuta on 11/21/2016.
 */
@Repository
public interface PermissionDao extends CrudRepository<Permission, Long> {
}
