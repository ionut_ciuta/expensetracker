package com.ciuta.ionut.dao;

import com.ciuta.ionut.entities.Currency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Repository
public interface CurrencyDao extends CrudRepository<Currency, Long>{
}
