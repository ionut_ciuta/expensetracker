package com.ciuta.ionut.dao;

import com.ciuta.ionut.entities.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Repository
public interface TransactionDao extends CrudRepository<Transaction, Long>{
}
