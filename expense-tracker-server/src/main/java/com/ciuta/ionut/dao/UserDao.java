package com.ciuta.ionut.dao;

import com.ciuta.ionut.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Ionut Ciuta on 11/21/2016.
 */
@Repository
public interface UserDao extends CrudRepository<User, Long> {
}
