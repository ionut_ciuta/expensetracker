package com.ciuta.ionut.dao;

import com.ciuta.ionut.entities.UserType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Ionut Ciuta on 11/21/2016.
 */
@Repository
public interface UserTypeDao extends CrudRepository<UserType, Long> {
}
