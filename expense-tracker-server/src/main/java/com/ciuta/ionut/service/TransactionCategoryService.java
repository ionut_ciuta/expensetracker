package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.TransactionCategory;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface TransactionCategoryService extends AbstractService<TransactionCategory, Long> {

}
