package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.Transaction;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface TransactionService extends AbstractService<Transaction, Long> {
}
