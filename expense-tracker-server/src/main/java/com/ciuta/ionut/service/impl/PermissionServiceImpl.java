package com.ciuta.ionut.service.impl;

import com.ciuta.ionut.dao.PermissionDao;
import com.ciuta.ionut.entities.Permission;
import com.ciuta.ionut.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionDao dao;

    @Override
    public List<Permission> findAll() {
        return (List<Permission>)dao.findAll();
    }

    @Override
    public Permission findOne(Long entityId) {
        return dao.findOne(entityId);
    }

    @Override
    public Permission save(Permission newEntity) {
        return dao.save(newEntity);
    }

    @Override
    public void remove(Permission toBeRemovedEntity) {
        dao.delete(toBeRemovedEntity);
    }
}
