package com.ciuta.ionut.service.impl;

import com.ciuta.ionut.dao.UserDao;
import com.ciuta.ionut.entities.User;
import com.ciuta.ionut.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao dao;

    @Override
    public List<User> findAll() {
        return (List<User>)dao.findAll();
    }

    @Override
    public User findOne(Long entityId) {
        return dao.findOne(entityId);
    }

    @Override
    public User save(User newEntity) {
        return dao.save(newEntity);
    }

    @Override
    public void remove(User toBeRemovedEntity) {
        dao.delete(toBeRemovedEntity);
    }
}
