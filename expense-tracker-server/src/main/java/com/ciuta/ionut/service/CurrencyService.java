package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.Currency;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface CurrencyService extends AbstractService<Currency, Long> {
}
