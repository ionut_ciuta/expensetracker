package com.ciuta.ionut.service.impl;

import com.ciuta.ionut.dao.UserTypeDao;
import com.ciuta.ionut.entities.UserType;
import com.ciuta.ionut.service.UserTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Service
public class UserTypeServiceImpl implements UserTypeService {
    @Autowired
    private UserTypeDao dao;

    @Override
    public List<UserType> findAll() {
        return (List<UserType>) dao.findAll();
    }

    @Override
    public UserType findOne(Long entityId) {
        return dao.findOne(entityId);
    }

    @Override
    public UserType save(UserType newEntity) {
        return dao.save(newEntity);
    }

    @Override
    public void remove(UserType toBeRemovedEntity) {
        dao.save(toBeRemovedEntity);
    }
}
