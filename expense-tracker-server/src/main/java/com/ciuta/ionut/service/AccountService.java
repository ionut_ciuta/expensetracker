package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.Account;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface AccountService extends AbstractService<Account, Long> {
}
