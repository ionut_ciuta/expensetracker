package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.Permission;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface PermissionService extends AbstractService<Permission, Long> {

}
