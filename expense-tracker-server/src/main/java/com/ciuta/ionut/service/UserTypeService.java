package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.UserType;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface UserTypeService extends AbstractService<UserType, Long> {
}
