package com.ciuta.ionut.service.impl;

import com.ciuta.ionut.dao.AccountDao;
import com.ciuta.ionut.entities.Account;
import com.ciuta.ionut.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountDao dao;

    @Override
    public List<Account> findAll() {
        return (List<Account>) dao.findAll();
    }

    @Override
    public Account findOne(Long entityId) {
        return dao.findOne(entityId);
    }

    @Override
    public Account save(Account newEntity) {
        return dao.save(newEntity);
    }

    @Override
    public void remove(Account toBeRemovedEntity) {
        dao.delete(toBeRemovedEntity);
    }
}
