package com.ciuta.ionut.service.impl;

import com.ciuta.ionut.dao.CurrencyDao;
import com.ciuta.ionut.entities.Currency;
import com.ciuta.ionut.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Service
public class CurrencyServiceImpl implements CurrencyService {
    @Autowired
    private CurrencyDao dao;

    @Override
    public List<Currency> findAll() {
        return (List<Currency>) dao.findAll();
    }

    @Override
    public Currency findOne(Long entityId) {
        return dao.findOne(entityId);
    }

    @Override
    public Currency save(Currency newEntity) {
        return dao.save(newEntity);
    }

    @Override
    public void remove(Currency toBeRemovedEntity) {
        dao.delete(toBeRemovedEntity);
    }
}
