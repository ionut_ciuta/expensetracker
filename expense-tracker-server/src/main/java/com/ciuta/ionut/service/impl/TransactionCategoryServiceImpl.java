package com.ciuta.ionut.service.impl;

import com.ciuta.ionut.dao.TransactionCategoryDao;
import com.ciuta.ionut.entities.TransactionCategory;
import com.ciuta.ionut.service.TransactionCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Service
public class TransactionCategoryServiceImpl implements TransactionCategoryService {
    @Autowired
    private TransactionCategoryDao dao;

    @Override
    public List<TransactionCategory> findAll() {
        return (List<TransactionCategory>) dao.findAll();
    }

    @Override
    public TransactionCategory findOne(Long entityId) {
        return dao.findOne(entityId);
    }

    @Override
    public TransactionCategory save(TransactionCategory newEntity) {
        return dao.save(newEntity);
    }

    @Override
    public void remove(TransactionCategory toBeRemovedEntity) {
        dao.delete(toBeRemovedEntity);
    }
}
