package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.AbstractEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface AbstractService<E extends AbstractEntity, K extends Serializable> {
    List<E> findAll();
    E findOne(K entityId);
    E save(E newEntity);
    void remove(E toBeRemovedEntity);
}
