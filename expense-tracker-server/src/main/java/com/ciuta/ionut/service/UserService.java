package com.ciuta.ionut.service;

import com.ciuta.ionut.entities.User;

/**
 * Ionut Ciuta on 12/1/2016.
 */
public interface UserService extends AbstractService<User, Long> {

}
