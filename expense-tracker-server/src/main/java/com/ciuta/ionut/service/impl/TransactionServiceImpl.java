package com.ciuta.ionut.service.impl;

import com.ciuta.ionut.dao.TransactionDao;
import com.ciuta.ionut.entities.Transaction;
import com.ciuta.ionut.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ionut Ciuta on 12/1/2016.
 */
@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    private TransactionDao dao;

    @Override
    public List<Transaction> findAll() {
        return (List<Transaction>) dao.findAll();
    }

    @Override
    public Transaction findOne(Long entityId) {
        return dao.findOne(entityId);
    }

    @Override
    public Transaction save(Transaction newEntity) {
        return dao.save(newEntity);
    }

    @Override
    public void remove(Transaction toBeRemovedEntity) {
        dao.save(toBeRemovedEntity);
    }
}
